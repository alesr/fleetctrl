# fleetctrl

## HowTo

- `make apispec-up` to start the API documentation Docker container and make it available at `http://127.0.0.1:4567`

Consult the API specification to see all the endpoints available in this API.

- `make up` to start the FleetCtrl API


## Available commands

```
------------------------------------------------------------------------
FLEETCTRL
------------------------------------------------------------------------
apispec-down                   stop and remove API specification container
apispec-up                     start the API specification container
build                          compile packages and dependencies
down                           stop and remove the service container
generate                       generate interface mocks.
lint                           run linter
test-it                        run integration tests
test                           test packages
tidy                           add missing and remove unused modules
up                             start the service
vendor                         make vendored copy of dependencies
```

## Project core organization

```
.
├── app
│   └── app.go
├── config
│   └── config.go
├── db
│   └── db.go
└── resource
    └── vehicle
        ├── handler.go
        ├── handler_default.go
        ├── handler_error.go
        ├── repository.go
        ├── repository_default.go
        ├── service.go
        └── service_default.go
```

`app.go` initializes services, schedulers and describe application API

`handler.go` and `handler_default.go` implements the transport interface and HTTP handlers for RESTful web service.

`service.go` and `service_default.go` implements the interface and domain logic for the vehicle resource using the `fleetlib` package.

`repository.go` and `repository_default.go` implements the interface and repository logic for manipulating storage.

`db.go` implements basic storage using `go map` to mimic a real database.

`config.go` loads configuration values from environment variables and prepare them for dependency injection.
