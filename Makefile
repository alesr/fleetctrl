PROJECT_NAME := FLEETCTRL

.PHONY: help
help:
	@echo "------------------------------------------------------------------------"
	@echo "${PROJECT_NAME}"
	@echo "------------------------------------------------------------------------"
	@grep -E '^[a-zA-Z0-9_/%\-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: apispec-up
apispec-up: ## start the API specification container
	docker-compose up api-spec

.PHONY: apispec-down
apispec-down: ## stop and remove API specification container
	docker-compose rm -svf api-spec

.PHONY: build
build: ## compile packages and dependencies
	go build -race

.PHONY: down
down: ## stop and remove the service container
	docker-compose rm -svf api-spec

.PHONY: generate
generate: ## generate interface mocks.
	go generate ./...

.PHONY: lint
lint: ## run linter
	golangci-lint run

.PHONY: test
test: ## test packages
	go test -race -v ./...

.PHONY: test-it
test-it: ## run integration tests
	docker-compose -f docker-compose.yml \
	               -f docker-compose.it-driver.yml \
	               up --build --abort-on-container-exit fleetctrl it-driver

.PHONY: tidy
tidy: ## add missing and remove unused modules
	go mod tidy

.PHONY: up
up: ## start the service
	docker-compose up \
	--exit-code-from fleetctrl \
	--remove-orphans \
	--build fleetctrl

.PHONY: vendor
vendor: ## make vendored copy of dependencies
	go mod vendor
