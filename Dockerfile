FROM golang:1.11

LABEL maintainer="Alessandro Resta <alessandro.resta@gmail.com>"

RUN mkdir -p /go/src/gitlab.com/alesr/fleetctrl
WORKDIR /go/src/gitlab.com/alesr/fleetctrl

COPY main.go main.go
COPY ./etc etc
COPY ./internal internal
COPY ./vendor vendor

ENV \
  PORT='443' \
  BASIC_AUTH_USER='dev' \
  BASIC_AUTH_PASS='lOZQsG+c5d9YfEgOr8d/uQ=='

EXPOSE 443

RUN go install -race

CMD ["fleetctrl"]
