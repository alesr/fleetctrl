package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"time"

	"gitlab.com/alesr/fleetctrl/internal/app"
	"gitlab.com/alesr/fleetctrl/internal/config"
)

func main() {
	conf := config.New()

	application := app.New(
		conf.Logger,
		conf.Port,
		conf.CertFile,
		conf.KeyFile,
		conf.BasicAuthUser,
		conf.BasicAuthPass,
		conf.RequestIDCtxKey,
	)

	go handleInterrupt(application)

	if err := application.Start(context.Background()); err != nil {
		log.Fatalf("could not start application: %s", err)
	}
}

func handleInterrupt(app *app.App) {
	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, os.Interrupt)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	for range signalCh {
		if err := app.Stop(ctx); err != nil {
			log.Fatalf("could not stop application: %s", err)
		}
		os.Exit(1)
	}
}
