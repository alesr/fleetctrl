module gitlab.com/alesr/fleetctrl

require (
	github.com/goji/httpauth v0.0.0-20160601135302-2da839ab0f4d
	github.com/google/uuid v1.1.0
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.2.2
	gitlab.com/alesr/fleetlib v1.1.0
	gopkg.in/robfig/cron.v2 v2.0.0-20150107220207-be2e0b0deed5
)
