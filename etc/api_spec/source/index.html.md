---
title: API Reference

language_tabs:
  - shell

toc_footers:
  - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - errors

search: true
---

# Introduction

Welcome to the FleetCtrl API! You can use our API to access FleetCtrl API endpoints, which can provide information on vehicles and change their states in our database.

# User roles and Vehicle states

These are the user roles currently supported by the FleetCtrl API.
Note that the information that can be retrieved or set on each API depends on the user role permission.

For instance, when requesting the list of vehicles the `rider` user role will only see vehicles in the following states `[ready, riding]`, `hunter` users: `[ready, riding, bounty, collected, and dropped]` while `admin` users will receive a list of vehicles with all states.

Here is the list of all vehicle's states supported by our API:

`
ready, battery_low, bounty, riding, collected, dropped, service_mode, terminated, unknown
`

# Authentication

> To authorize, use this code:

```shell
# With shell, you can just pass the correct header with each request
curl -u user:password "api_endpoint_here"
```

FleetCtrl uses Basic Authentication to allow access to the API.

<aside class="notice">
For local development use the credentials <code>dev:lOZQsG+c5d9YfEgOr8d/uQ==</code> with -k or --insecure flags.
</aside>

# Vehicles

## Get a New Vehicle

```shell
curl -ku dev:lOZQsG+c5d9YfEgOr8d/uQ== \
  https://localhost:8088/api/v1/vehicle/new?role=admin
```

This endpoint allow admins to request the creation of a new vehicle instance.

### HTTP Request

`GET /api/v1/vehicle/new`

### URL Parameters

Parameter | Default | Description
--------- | ------- | -----------
role | no default | User roles: admin

> Success:

```

200 - OK
```

> Example response body:

```json
{
  "id": "2470a6ad-6c24-48f0-a036-aa2854ab885e",
  "serial_number": "BGH464PKD5F833KBU140",
  "category": "scooter",
  "battery_level": "60",
  "state": "ready",
  "operational": true,
  "last_modified": "2018-12-25 15:42.2311228 +0000 UTC"
}
```

> Errors:

```
Error Code | Meaning
401 | Unauthorized -- invalid api credentials
403 | Forbidden -- unauthorized user role
```

## Get All vehicles

This endpoint retrieves all vehicles for a given user role.

```shell
curl -ku dev:lOZQsG+c5d9YfEgOr8d/uQ== https://localhost:8088/api/v1/vehicles?role=admin
```

### HTTP Request

`GET /api/v1/vehicles`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
role | no default | User roles: rider, hunter, admin

> Success:

```

200 - OK
```

> Example response body:

```json
{
  "vehicles": [
    {
      "id": "6a9cb1fd-32e2-4d44-a60a-ee130f5153c5",
      "serial_number": "BGH3ET9KD5F833K9VS1G",
      "category": "scooter",
      "battery_level": "100",
      "state": "service_mode",
      "operational": false,
      "last_modified": "2018-12-25 13:58:13.4351996 +0000 UTC"
    },
    {
      "id": "657d6d1a-288a-40eb-b449-36f42d0071ad",
      "serial_number": "BGH3ET9KD5F833K9VSA0",
      "category": "scooter",
      "battery_level": "70",
      "state": "ready",
      "operational": true,
      "last_modified": "2018-12-25 13:58:13.4362462 +0000 UTC"
    },
    {
      "id": "11ea5354-15de-42e1-8933-7d701d9a1df0",
      "serial_number": "BGH3ET9KD5F833K9VS30",
      "category": "scooter",
      "battery_level": "10",
      "state": "bounty",
      "operational": true,
      "last_modified": "2018-12-25 13:58:13.4353868 +0000 UTC"
    }
  ]
}
```

> Errors:

```
Error Code | Meaning
400 | Bad Request -- missing user role
401 | Unauthorized -- invalid api credentials
422 | Unprocessable Entity -- unsupported user role
```


## Get a Specific Vehicle

```shell
curl -ku dev:lOZQsG+c5d9YfEgOr8d/uQ== \
  https://localhost:8088/api/v1/vehicle/6a9cb1fd-32e2-4d44-a60a-ee130f5153c5?role=rider
```

This endpoint retrieves a specific vehicle for a given user role.


### HTTP Request

`GET /api/v1/vehicles/<ID>`

### URL Parameters

Parameter | Default | Description
--------- | ------- | -----------
role | no default | User roles: rider, hunter, admin

> Success:

```

200 - OK
```

> Example response body:

```json
{
  "id": "2470a6ad-6c24-48f0-a036-aa2854ab885e",
  "serial_number": "BGH464PKD5F833KBU140",
  "category": "scooter",
  "battery_level": "60",
  "state": "ready",
  "operational": true,
  "last_modified": "2018-12-25 15:42.2311228 +0000 UTC"
}
```

> Errors:

```
Error Code | Meaning
400 | Bad Request -- missing user role
400 | Bad Request -- missing vehicle id
401 | Unauthorized -- invalid api credentials
404 | Not Found -- vehicle not found
422 | Unprocessable Entity -- unsupported user role
```

## Set the State of a Vehicle

```shell
curl -v -ku dev:lOZQsG+c5d9YfEgOr8d/uQ== \
  -X PATCH -H "Content-Type: application/json" \
  -d '{"id": "06ed3c2d-1a6c-4f00-916e-5e097cb9c672", "state": "terminated"}' \
  -v -k https://localhost:8088/api/v1/vehicle/state?role=admin
```

This endpoint sets the state of a given vehicle.


### HTTP Request

`PATCH /api/v1/vehicle/state`

### URL Parameters

Parameter | Default | Description
--------- | ------- | -----------
role | no default | User roles: rider, hunter, admin

> Example request body:

```json
{
  "id": "06ed3c2d-1a6c-4f00-916e-5e097cb9c672",
  "state": "terminated"
}

```

> Success:

```

200 - OK
```

> Example response body:

```json
{
  "id": "06ed3c2d-1a6c-4f00-916e-5e097cb9c672",
  "serial_number": "BPK129ELD5F833LNK151",
  "category": "scooter",
  "battery_level": "100",
  "state": "terminated",
  "operational": false,
  "last_modified": "2018-12-25 15:42:47.9511228 +0000 UTC"
}
```

> Errors:

```
Error Code | Meaning
400 | Bad Request -- missing user role
400 | Bad Request -- missing vehicle id
401 | Unauthorized -- invalid api credentials
403 | Forbidden -- unauthorized user role
404 | Not Found -- vehicle not found
409 | Conflict -- vehicle battery level is too low
422 | Unprocessable Entity -- unsupported user role
422 | Unprocessable Entity -- unsupported vehicle state
422 | Unprocessable Entity -- unsupported vehicle state transition
```
