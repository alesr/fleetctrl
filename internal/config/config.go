// Config package holds all the configurations relative to an application.
// The environment variables are often added to Dockerfile, docker-compose  or on our cloud provider settings.
// We don't want misconfigured services out there so if anything goes wrong we panic right away!

package config

import (
	"fmt"
	"net"
	"os"

	"github.com/sirupsen/logrus"
)

// RequestIDContextKey defines the data type for the context key.
type RequestIDContextKey string

// Config defines the data type for service configuration.
type Config struct {
	Logger          *Logger
	Port            string
	CertFile        string
	KeyFile         string
	BasicAuthUser   string
	BasicAuthPass   string
	RequestIDCtxKey RequestIDContextKey
}

// Logger defines the application logger. Having the external logger embedded
// on our own struct prevent callers to have to import the external logger.
type Logger struct {
	*logrus.Logger
}

// New instantiates a new service configuration.
func New() *Config {
	return &Config{
		Logger:          newLogger(lookupEnvar("LOG_LEVEL")),
		Port:            net.JoinHostPort("", lookupEnvar("PORT")),
		CertFile:        lookupEnvar("CERT_FILE"),
		KeyFile:         lookupEnvar("KEY_FILE"),
		BasicAuthUser:   lookupEnvar("BASIC_AUTH_USER"),
		BasicAuthPass:   lookupEnvar("BASIC_AUTH_PASS"),
		RequestIDCtxKey: RequestIDContextKey("request-id"),
	}
}

func newLogger(lvl string) *Logger {
	logger := logrus.New()

	logger.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		PrettyPrint:     false,
	})

	logger.SetReportCaller(true)
	logger.Out = os.Stdout

	logLvl, err := logrus.ParseLevel(lvl)
	if err != nil {
		panic(fmt.Sprintf("could not parse log level: %s", err))
	}

	logger.SetLevel(logLvl)
	return &Logger{logger}
}

func lookupEnvar(envar string) string {
	val, found := os.LookupEnv(envar)
	if !found {
		panic(fmt.Sprintf("could not lookup log level environment variable: '%s'", envar))
	}
	return val
}
