package vehicle

import (
	"strconv"
)

type listResponse struct {
	Items []itemResponse `json:"vehicles,omitempty"`
}

type itemResponse struct {
	ID           string `json:"id"`
	SerialNumber string `json:"serial_number"`
	Category     string `json:"category"`
	BatteryLevel string `json:"battery_level"`
	State        string `json:"state"`
	Operational  bool   `json:"operational"`
	LastModified string `json:"last_modified"`
}

type setStateRequest struct {
	ID    string `json:"id"`
	State string `json:"state"`
}

func (s setStateRequest) validate() error {
	if s.ID == "" {
		return errMissingVehicleID
	}
	if s.State == "" {
		return errMissingVehicleState
	}
	return nil
}

func newItemResponseFromDomain(d DomainModel) itemResponse {
	return itemResponse{
		ID:           d.ID.String(),
		SerialNumber: d.Vehicle.SerialNumber(),
		Category:     d.Vehicle.Category(),
		BatteryLevel: strconv.Itoa(d.Vehicle.BatteryLevel()),
		State:        d.Vehicle.State(),
		Operational:  d.Vehicle.Operational(),
		LastModified: d.Vehicle.LastModified().String(),
	}
}
