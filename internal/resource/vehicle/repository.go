package vehicle

import (
	"gitlab.com/alesr/fleetctrl/internal/db"
)

//go:generate moq -out repository_mock.go . Repository

// Repository defines the repository interface.
type Repository interface {
	Get(id string) *db.StorageModel
	Insert(id string, s db.StorageModel)
	List(states []string) []db.StorageModel
	Update(id string, s db.StorageModel)
}
