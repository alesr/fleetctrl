package vehicle

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/alesr/fleetctrl/internal/db"
	"gitlab.com/alesr/fleetlib"
)

func TestGet(t *testing.T) {
	givenID := "dummy-id"

	expectedStorageModel := db.StorageModel{
		ID:      "dummy-id",
		Vehicle: &fleetlib.Scooter{},
	}

	mockDB := db.DatabaseMock{
		GetFunc: func(id string) *db.StorageModel {
			if id == givenID {
				return &expectedStorageModel
			}
			return nil
		},
	}

	repo := NewDefaultRepository(nil, &mockDB)

	actual := repo.Get(givenID)
	assert.Equal(t, &expectedStorageModel, actual)
}

func TestInsert(t *testing.T) {
	givenID := "dummy-id"
	expectedID := "dummy-id"

	givenStorageModel := db.StorageModel{
		ID:      "dummy-id",
		Vehicle: &fleetlib.Scooter{},
	}

	expectedStorageModel := db.StorageModel{
		ID:      "dummy-id",
		Vehicle: &fleetlib.Scooter{},
	}

	mockDB := db.DatabaseMock{
		InsertFunc: func(id string, s db.StorageModel) {
			assert.Equal(t, expectedID, id)
			assert.Equal(t, expectedStorageModel, s)
		},
	}

	repo := NewDefaultRepository(nil, &mockDB)
	repo.Insert(givenID, givenStorageModel)
}

func TestList(t *testing.T) {
	vehicleMock1 := fleetlib.VehicleMock{
		StateFunc: func() string { return "stateA" },
	}
	vehicleMock2 := fleetlib.VehicleMock{
		StateFunc: func() string { return "stateB" },
	}
	vehicleMock3 := fleetlib.VehicleMock{
		StateFunc: func() string { return "stateC" },
	}

	mockDB := db.DatabaseMock{
		ListFunc: func() []db.StorageModel {
			return []db.StorageModel{
				{
					ID:      "dummy-id-1",
					Vehicle: &vehicleMock1,
				},
				{
					ID:      "dummy-id-2",
					Vehicle: &vehicleMock2,
				},
				{
					ID:      "dummy-id-3",
					Vehicle: &vehicleMock3,
				},
			}
		},
	}

	// doesn't inclide vehicle with stateB
	givenStatesToList := []string{"stateA", "stateC"}

	expectedStorageModels := []db.StorageModel{
		{
			ID:      "dummy-id-1",
			Vehicle: &vehicleMock1,
		},
		{
			ID:      "dummy-id-3",
			Vehicle: &vehicleMock3,
		},
	}

	repo := NewDefaultRepository(nil, &mockDB)

	actual := repo.List(givenStatesToList)
	assert.Equal(t, expectedStorageModels, actual)
}

func TestUpdate(t *testing.T) {
	givenID := "dummy-id"
	expectedID := "dummy-id"

	givenStorageModel := db.StorageModel{
		ID:      "dummy-id",
		Vehicle: &fleetlib.Scooter{},
	}

	expectedStorageModel := db.StorageModel{
		ID:      "dummy-id",
		Vehicle: &fleetlib.Scooter{},
	}

	mockDB := db.DatabaseMock{
		UpdateFunc: func(id string, s db.StorageModel) string {
			assert.Equal(t, expectedID, id)
			assert.Equal(t, expectedStorageModel, s)
			return ""
		},
	}

	repo := NewDefaultRepository(nil, &mockDB)
	repo.Update(givenID, givenStorageModel)
}

func TestContain(t *testing.T) {
	cases := []struct {
		name      string
		givenList []string
		givenItem string
		expected  bool
	}{
		{
			name:      "notInList",
			givenList: []string{"foo", "bar"},
			givenItem: "baz",
			expected:  false,
		},
		{
			name:      "inList",
			givenList: []string{"foo", "bar", "qux"},
			givenItem: "qux",
			expected:  true,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := contain(tc.givenList, tc.givenItem)
			assert.Equal(t, tc.expected, actual)
		})
	}
}
