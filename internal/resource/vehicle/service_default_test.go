package vehicle

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPast48hSinceLastStateChange(t *testing.T) {
	t.Parallel()

	currentTime, err := time.Parse(time.RFC3339, "2018-12-29T10:00:00+00:00")
	require.NoError(t, err)

	lastStateChange1, err := time.Parse(time.RFC3339, "2018-12-30T11:10:00+00:00")
	require.NoError(t, err)

	lastStateChange2, err := time.Parse(time.RFC3339, "2018-12-31T10:00:00+00:00")
	require.NoError(t, err)

	lastStateChange3, err := time.Parse(time.RFC3339, "2018-12-31T10:00:01+00:00")
	require.NoError(t, err)

	cases := []struct {
		name            string
		currentTime     time.Time
		lastStateChange time.Time
		expected        bool
	}{
		{
			name:            "lessThen48h",
			currentTime:     currentTime,
			lastStateChange: lastStateChange1,
			expected:        false,
		},
		{
			name:            "exactly48h",
			currentTime:     currentTime,
			lastStateChange: lastStateChange2,
			expected:        false,
		},
		{
			name:            "moreThen48h",
			currentTime:     currentTime,
			lastStateChange: lastStateChange3,
			expected:        true,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := past48hSinceLastStateChange(tc.currentTime, tc.lastStateChange)

			assert.Equal(t, tc.expected, actual)
		})
	}
}
