package vehicle

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/alesr/fleetlib"

	"github.com/sirupsen/logrus"
)

var (
	// Emumerate possible API errors.

	errMissingUserRole     = errors.New("missing user role")
	errMissingVehicleID    = errors.New("missing vehicle id")
	errMissingVehicleState = errors.New("missing vehicle state")
	errInvalidRequestBody  = errors.New("invalid request body")
)

type restError struct {
	Code    int    `json:"code"`
	Status  string `json:"status"`
	Message string `json:"message,omitempty"`
}

func newRestError(code int, status, msg string) restError {
	return restError{
		Code:    code,
		Status:  status,
		Message: msg,
	}
}

func (h *DefaultHandler) newError(w http.ResponseWriter, r *http.Request, err error) {

	var restErr restError

	switch err {
	case errMissingUserRole,
		errMissingVehicleID,
		errMissingVehicleState,
		errInvalidRequestBody:
		code := http.StatusBadRequest
		w.WriteHeader(code)
		restErr = newRestError(code, http.StatusText(code), err.Error())

	case errVehicleNotFound:
		code := http.StatusNotFound
		w.WriteHeader(code)
		restErr = newRestError(code, http.StatusText(code), err.Error())

	case errUnauthorizedUserRole,
		fleetlib.ErrUnauthorizedUserRole:
		code := http.StatusForbidden
		w.WriteHeader(code)
		restErr = newRestError(code, http.StatusText(code), err.Error())

	case fleetlib.ErrUnsupportedUserRole,
		fleetlib.ErrUnsupportedVehicleState,
		fleetlib.ErrUnprocessableVehicleStateTransition:
		code := http.StatusUnprocessableEntity
		w.WriteHeader(code)
		restErr = newRestError(code, http.StatusText(code), err.Error())

	case fleetlib.ErrVehicleBatteryLevelTooLow:
		code := http.StatusConflict
		w.WriteHeader(code)
		restErr = newRestError(code, http.StatusText(code), err.Error())

	default:
		code := http.StatusInternalServerError
		w.WriteHeader(code)
		restErr = newRestError(code, http.StatusText(code), "")
	}

	reqID := r.Context().Value(h.reqIDCtxKey).(string)
	h.logger.WithFields(logrus.Fields{
		"request-id": reqID,
		"code":       restErr.Code,
		"status":     restErr.Status,
		"message":    restErr.Message,
	}).Error("sending error")

	if err := json.NewEncoder(w).Encode(&restErr); err != nil {
		h.logger.WithField("request-id", reqID).Errorln("could not write error")
		_, _ = fmt.Fprint(w, restError{
			Code:   http.StatusInternalServerError,
			Status: http.StatusText(http.StatusInternalServerError),
		})
	}
}
