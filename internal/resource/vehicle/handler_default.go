package vehicle

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/alesr/fleetctrl/internal/config"
)

// DefaultHandler defines the HTTP transport layer for the resource Vehicle.
type DefaultHandler struct {
	logger      *config.Logger
	service     Service
	reqIDCtxKey config.RequestIDContextKey
}

// NewDefaultHandler instantiates a new default handler.
func NewDefaultHandler(
	logger *config.Logger,
	service Service,
	reqIDCtxKey config.RequestIDContextKey) *DefaultHandler {
	return &DefaultHandler{
		logger:      logger,
		service:     service,
		reqIDCtxKey: reqIDCtxKey,
	}
}

// NewVehicle is a GET handler for requesting the creation of a new vehicle.
func (h *DefaultHandler) NewVehicle(w http.ResponseWriter, r *http.Request) {
	resp, err := h.newVehicle(r)
	if err != nil {
		h.newError(w, r, err)
		return
	}
	h.writeResponse(w, r, resp)
}

func (h *DefaultHandler) newVehicle(r *http.Request) (*itemResponse, error) {
	userRole := r.URL.Query().Get("role")
	if userRole == "" {
		return nil, errMissingUserRole
	}

	result, err := h.service.CreateVehicle(userRole)
	if err != nil {
		return nil, err
	}

	resp := newItemResponseFromDomain(*result)

	return &resp, nil
}

// GetVehicle is a GET handler for getting a vehicle given its id.
func (h *DefaultHandler) GetVehicle(w http.ResponseWriter, r *http.Request) {
	resp, err := h.getVehicle(r)
	if err != nil {
		h.newError(w, r, err)
		return
	}
	h.writeResponse(w, r, resp)
}

func (h *DefaultHandler) getVehicle(r *http.Request) (*itemResponse, error) {
	userRole := r.URL.Query().Get("role")
	if userRole == "" {
		return nil, errMissingUserRole
	}

	vehicleID := mux.Vars(r)["id"]
	if vehicleID == "" {
		return nil, errMissingVehicleID
	}

	result, err := h.service.GetVehicle(userRole, vehicleID)
	if err != nil {
		return nil, err
	}

	resp := newItemResponseFromDomain(*result)

	return &resp, nil
}

// ListVehicles is a GET handler for listing vehicles for a given user.
func (h *DefaultHandler) ListVehicles(w http.ResponseWriter, r *http.Request) {
	resp, err := h.listVehicles(r)
	if err != nil {
		h.newError(w, r, err)
		return
	}
	h.writeResponse(w, r, resp)
}

func (h *DefaultHandler) listVehicles(r *http.Request) (*listResponse, error) {
	userRole := r.URL.Query().Get("role")
	result, err := h.service.ListVehicles(userRole)
	if err != nil {
		return nil, err
	}

	vehicles := make([]itemResponse, 0)

	for _, r := range result {
		vehicles = append(vehicles, newItemResponseFromDomain(r))
	}

	return &listResponse{
		Items: vehicles,
	}, nil
}

// SetVehicleState is a PATCH handler for setting a vehicle to a given state.
func (h *DefaultHandler) SetVehicleState(w http.ResponseWriter, r *http.Request) {
	resp, err := h.setVehicleState(r)
	if err != nil {
		h.newError(w, r, err)
		return
	}
	h.writeResponse(w, r, resp)
}

func (h *DefaultHandler) setVehicleState(r *http.Request) (*itemResponse, error) {
	userRole := r.URL.Query().Get("role")
	if userRole == "" {
		return nil, errMissingUserRole
	}

	var reqData setStateRequest
	if err := json.NewDecoder(r.Body).Decode(&reqData); err != nil {
		return nil, errInvalidRequestBody
	}

	if err := reqData.validate(); err != nil {
		return nil, err
	}

	result, err := h.service.SetVehicleState(userRole, reqData.ID, reqData.State)
	if err != nil {
		return nil, err
	}

	resp := newItemResponseFromDomain(*result)
	return &resp, nil
}

func (h *DefaultHandler) writeResponse(w http.ResponseWriter, r *http.Request, data interface{}) {
	if err := json.NewEncoder(w).Encode(data); err != nil {
		reqID := r.Context().Value(h.reqIDCtxKey).(string)

		h.logger.WithField("request-id", reqID).Errorln("could not write response")
		_, _ = fmt.Fprint(w, restError{
			Code:   http.StatusInternalServerError,
			Status: http.StatusText(http.StatusInternalServerError),
		})
	}
}
