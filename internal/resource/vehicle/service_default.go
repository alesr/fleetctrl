package vehicle

import (
	"math/rand"
	"time"

	"github.com/google/uuid"
	"gitlab.com/alesr/fleetctrl/internal/config"
	"gitlab.com/alesr/fleetlib"

	cron "gopkg.in/robfig/cron.v2"
)

var _ Service = (*DefaultService)(nil)

// DefaultService implements the Service interface.
type DefaultService struct {
	logger     *config.Logger
	repository Repository
}

// NewDefaultService instantiates a new default service.
func NewDefaultService(logger *config.Logger, repository Repository) *DefaultService {
	return &DefaultService{logger: logger, repository: repository}
}

// PopulateDB is a convenience function to add some sample data to the database.
func (s *DefaultService) PopulateDB() {
	rand.Seed(time.Now().UnixNano())
	numOfSampleVehicles := rand.Intn(50)

	var domainModels []DomainModel

	for i := 0; i <= numOfSampleVehicles; i++ {
		scooter := fleetlib.NewScooter()
		scooter.SetRandState()

		domainModels = append(domainModels, DomainModel{
			ID:      uuid.New(),
			Vehicle: scooter,
		})
	}

	for _, d := range domainModels {
		s.repository.Insert(d.ID.String(), newStorageModelFromDomain(d))
	}
}

// CreateVehicle creates a new vehicle in service mode.
func (s *DefaultService) CreateVehicle(userRole string) (*DomainModel, error) {
	if userRole != fleetlib.UserRoleAdmin {
		return nil, errUnauthorizedUserRole
	}

	d := DomainModel{
		ID:      uuid.New(),
		Vehicle: fleetlib.NewScooter(),
	}

	storage := newStorageModelFromDomain(d)

	s.repository.Insert(d.ID.String(), storage)

	return &d, nil
}

// GetVehicle gets a vehicle given its ID.
func (s *DefaultService) GetVehicle(userRole, vehicleID string) (*DomainModel, error) {
	entry := s.repository.Get(vehicleID)
	if entry == nil {
		return nil, errVehicleNotFound
	}

	d := newDomainModelFromStorage(*entry)

	allowedStates, err := fleetlib.UserRoleAllowedVehicleStates(userRole)
	if err != nil {
		return nil, err
	}

	for _, allowedState := range allowedStates {
		if d.Vehicle.State() == allowedState {
			return &d, nil
		}
	}
	return nil, errVehicleNotFound
}

// ListVehicles lists all vehicles for a given user role.
func (s *DefaultService) ListVehicles(userRole string) ([]DomainModel, error) {
	allowedStates, err := fleetlib.UserRoleAllowedVehicleStates(userRole)
	if err != nil {
		return nil, err
	}

	entries := s.repository.List(allowedStates)

	domainModels := make([]DomainModel, 0)

	for _, v := range entries {
		domainModels = append(domainModels, newDomainModelFromStorage(v))
	}
	return domainModels, nil
}

// SetVehicleState sets a vehicle to a given state taking in consideration the role permissions.
func (s *DefaultService) SetVehicleState(userRole, id, newState string) (*DomainModel, error) {
	entry := s.repository.Get(id)
	if entry == nil {
		return nil, errVehicleNotFound
	}

	d := newDomainModelFromStorage(*entry)

	if err := d.Vehicle.SetState(userRole, newState); err != nil {
		return nil, err
	}

	s.repository.Update(id, newStorageModelFromDomain(d))
	return &d, nil
}

// StartSchedulers starts the bounty and unknown state schedulers.
func (s *DefaultService) StartSchedulers() error {
	c := cron.New()

	_, err := c.AddFunc("TZ=Europe/Stockholm 30 21 * * * *", func() {
		if err := s.bountyStateScheduler(); err != nil {
			s.logger.Errorln("could not start auto bounty scheduler:", err)
		}
	})
	if err != nil {
		return err
	}

	_, err = c.AddFunc("@daily", func() {
		if err := s.unknownStateScheduler(); err != nil {
			s.logger.Errorln("could not start auto unknown scheduler:", err)
		}
	})
	if err != nil {
		return err
	}

	c.Start()
	return nil
}

func (s *DefaultService) bountyStateScheduler() error {
	domains, err := s.ListVehicles(fleetlib.UserRoleAdmin)
	if err != nil {
		return err
	}

	for _, d := range domains {
		d.Vehicle.SetAutoBounty()
	}
	return nil
}

func (s *DefaultService) unknownStateScheduler() error {
	currentTime := time.Now()

	domains, err := s.ListVehicles(fleetlib.UserRoleAdmin)
	if err != nil {
		return err
	}

	for _, d := range domains {
		if past48hSinceLastStateChange(currentTime, d.Vehicle.LastModified()) {
			d.Vehicle.SetAutoUnknown()
		}

	}
	return nil
}

func past48hSinceLastStateChange(currentTime, lastModified time.Time) bool {
	hoursSinceLastChange := lastModified.Sub(currentTime)
	return hoursSinceLastChange.Hours() > 48
}
