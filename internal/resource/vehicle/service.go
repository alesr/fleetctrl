package vehicle

import (
	"errors"

	"github.com/google/uuid"
	"gitlab.com/alesr/fleetctrl/internal/db"
	"gitlab.com/alesr/fleetlib"
)

// Enumerate possible service errors.

var (
	errVehicleNotFound      = errors.New("vehicle not found")
	errUnauthorizedUserRole = errors.New("unauthorized user role")
)

//go:generate moq -out service_mock.go . Service

// Service defines the service interface.
type Service interface {
	CreateVehicle(userRole string) (*DomainModel, error)
	GetVehicle(userRole, vehicleID string) (*DomainModel, error)
	ListVehicles(userRole string) ([]DomainModel, error)
	SetVehicleState(userRole, id, state string) (*DomainModel, error)
}

// DomainModel defines the service domain model.
type DomainModel struct {
	ID      uuid.UUID
	Vehicle fleetlib.Vehicle
}

func newDomainModelFromStorage(v db.StorageModel) DomainModel {
	id := uuid.MustParse(v.ID)
	return DomainModel{ID: id, Vehicle: v.Vehicle}
}

func newStorageModelFromDomain(d DomainModel) db.StorageModel {
	return db.StorageModel{
		ID:      d.ID.String(),
		Vehicle: d.Vehicle,
	}
}
