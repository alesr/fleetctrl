package vehicle

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/alesr/fleetctrl/internal/db"
	"gitlab.com/alesr/fleetlib"
)

func TestNewDomainModelFromStorage(t *testing.T) {
	vehicleMock := fleetlib.VehicleMock{
		SerialNumberFunc: func() string {
			return "dummy-serial-number"
		},
	}

	givenStorage := db.StorageModel{
		ID:      uuid.New().String(),
		Vehicle: &vehicleMock,
	}

	actual := newDomainModelFromStorage(givenStorage)

	assert.Equal(t, uuid.MustParse(givenStorage.ID), actual.ID)
	assert.Equal(t, givenStorage.Vehicle.SerialNumber(), actual.Vehicle.SerialNumber())
}

func TestNewStorageModelFromDomain(t *testing.T) {
	vehicleMock := fleetlib.VehicleMock{
		SerialNumberFunc: func() string {
			return "dummy-serial-number"
		},
	}

	givenDomain := DomainModel{
		ID:      uuid.New(),
		Vehicle: &vehicleMock,
	}

	actual := newStorageModelFromDomain(givenDomain)

	assert.Equal(t, givenDomain.ID.String(), actual.ID)
	assert.Equal(t, givenDomain.Vehicle.SerialNumber(), actual.Vehicle.SerialNumber())
}
