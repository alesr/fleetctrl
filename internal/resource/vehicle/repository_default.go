package vehicle

import (
	"gitlab.com/alesr/fleetctrl/internal/config"
	"gitlab.com/alesr/fleetctrl/internal/db"
)

var _ Repository = (*DefaultRepository)(nil)

// DefaultRepository implements the repository interface.DefaultRepository
// Note: In this prototype we're using a Go Map as our default repo.
type DefaultRepository struct {
	logger *config.Logger
	db     db.Database
}

// NewDefaultRepository instantiates a new default repository.
func NewDefaultRepository(logger *config.Logger, db db.Database) *DefaultRepository {
	return &DefaultRepository{logger: logger, db: db}
}

// Get gets a vehicle entry given its ID.
func (r *DefaultRepository) Get(id string) *db.StorageModel {
	return r.db.Get(id)
}

// Insert inserts a new vehicle entry to the database.
func (r *DefaultRepository) Insert(id string, s db.StorageModel) {
	r.db.Insert(id, s)
}

// List lists all vehicles entries in  the database with desidered states.
func (r *DefaultRepository) List(states []string) []db.StorageModel {
	entries := r.db.List()
	result := make([]db.StorageModel, 0)

	for _, entry := range entries {
		if contain(states, entry.Vehicle.State()) {
			result = append(result, entry)
		}
	}
	return result
}

// Update updates a vehicle entry in a the database.
func (r *DefaultRepository) Update(id string, s db.StorageModel) {
	_ = r.db.Update(id, s)
}

func contain(list []string, s string) bool {
	for _, v := range list {
		if v == s {
			return true
		}
	}
	return false
}
