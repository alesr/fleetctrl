package db

import (
	"fmt"
	"sync"

	"gitlab.com/alesr/fleetlib"
)

//go:generate moq -out db_mock.go . Database

// Database implements the interface for a database.
type Database interface {
	Get(id string) *StorageModel
	Insert(id string, s StorageModel)
	List() []StorageModel
	Update(id string, s StorageModel) string
}

// StorageModel defines the data model for DB entries.
type StorageModel struct {
	ID      string
	Vehicle fleetlib.Vehicle
}

var _ Database = (*FakeDB)(nil)

// FakeDB mimiking entries on a single table database.
type FakeDB struct {
	Storage map[string]StorageModel
	Mutex   *sync.Mutex
}

// NewFakeDB is a convenience function that return a Map with random data.
func NewFakeDB() *FakeDB {
	return &FakeDB{
		Storage: make(map[string]StorageModel),
		Mutex:   &sync.Mutex{},
	}
}

// Get gets an entry with a specific ID.
func (f *FakeDB) Get(id string) *StorageModel {
	f.Mutex.Lock()
	defer f.Mutex.Unlock()

	for k, v := range f.Storage {
		if k == id {
			return &v
		}
	}
	return nil
}

// Insert inserts an entry to the DB.
func (f *FakeDB) Insert(id string, s StorageModel) {
	f.Mutex.Lock()
	defer f.Mutex.Unlock()

	f.Storage[id] = s
}

// List list all entries.
func (f *FakeDB) List() []StorageModel {
	f.Mutex.Lock()
	defer f.Mutex.Unlock()

	entries := make([]StorageModel, 0)
	for _, v := range f.Storage {
		entries = append(entries, v)
	}
	return entries
}

// Update updates an entry given its id.
func (f *FakeDB) Update(id string, s StorageModel) string {
	f.Mutex.Lock()
	defer f.Mutex.Unlock()

	var rowsAffected int
	for k := range f.Storage {
		if k == id {
			f.Storage[k] = s
			rowsAffected++
		}
	}
	return fmt.Sprintf("%d rows affected", rowsAffected)
}
