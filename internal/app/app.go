// application package holds the implemantation of our service application.
// In this case a HTTP server.

package app

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"fmt"

	"gitlab.com/alesr/fleetctrl/internal/resource/vehicle"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/alesr/fleetctrl/internal/config"
	"gitlab.com/alesr/fleetctrl/internal/db"
	"github.com/goji/httpauth"
)

type middleware struct {
	logger *config.Logger
	reqIDCtxKey config.RequestIDContextKey
}

// App defines the service application.
type App struct {
	logger   *config.Logger
	port     string
	certFile string
	keyFile  string
	basicAuthUser string
	basicAuthPass string
	reqIDCtxKey config.RequestIDContextKey
}

// New instantiates a new App.
func New(
	logger *config.Logger,
	port string,
	certFile string,
	keyFile string,
	basicAuthUser string,
	basicAuthPass string,
	reqIDCtxKey config.RequestIDContextKey,
) *App {
	return &App{
		logger:   logger,
		port:     port,
		certFile: certFile,
		keyFile:  keyFile,
		basicAuthUser :basicAuthUser,
		basicAuthPass :	basicAuthPass,
		reqIDCtxKey: reqIDCtxKey,
	}
}

// Start starts the application.
func (a *App) Start(ctx context.Context) error {
	a.logger.Warn("starting application")

	// Repositories

	vehicleRepository := vehicle.NewDefaultRepository(
		a.logger,
		db.NewFakeDB(),
	)

	// Services

	vehicleService := vehicle.NewDefaultService(
		a.logger,
		 vehicleRepository,
		)

	// Resources

	vehicleHandler := vehicle.NewDefaultHandler(
		a.logger,
		 vehicleService,
			a.reqIDCtxKey,
		)

	vehicleService.PopulateDB()
	 if err := vehicleService.StartSchedulers(); err != nil {
		 return fmt.Errorf("could not start schedulers: %s", err)
	 }

	r := mux.NewRouter()
	r.StrictSlash(true)
	r.Use(httpauth.SimpleBasicAuth(a.basicAuthUser, a.basicAuthPass))

	mid := middleware{a.logger,a.reqIDCtxKey}

	r.Use(mid.reqIDMiddleware)
	r.Use(mid.logMiddleware)

	s := r.PathPrefix("/api/v1").Subrouter()

	rolePattern := "{role:[a-z]{5,6}}"

	s.HandleFunc("/vehicle/new", vehicleHandler.NewVehicle).Methods(http.MethodGet).Queries("role", rolePattern)
	s.HandleFunc("/vehicle/{id}", vehicleHandler.GetVehicle).Methods(http.MethodGet).Queries("role", rolePattern)
	s.HandleFunc("/vehicles", vehicleHandler.ListVehicles).Methods(http.MethodGet).Queries("role", rolePattern)
	s.HandleFunc("/vehicle/state", vehicleHandler.SetVehicleState).Methods(http.MethodPatch).Queries("role", rolePattern)

	return http.ListenAndServeTLS(a.port, a.certFile, a.keyFile, r)
}

// Stop stop the application.
// It will be useful with a real DB.
func (a *App) Stop(ctx context.Context) error {
	a.logger.Warn("stopping application")
	return nil
}



// TODO: Move this to the reverse proxy.
func (m middleware) reqIDMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), m.reqIDCtxKey, uuid.New().String())
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}


// TODO: Move this to the reverse proxy.
// TODO: Do not print user sensible data on production.
func (m middleware) logMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestID := r.Context().Value(m.reqIDCtxKey)

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			errMsg := "could not read request body"
			m.logger.WithField("request-id", requestID).Errorln(errMsg)
			http.Error(w, errMsg, http.StatusBadRequest)
		}

		if err := r.Body.Close(); err != nil {
			m.logger.Warn("could not close request body")
		}

		m.logger.WithFields(
			logrus.Fields{
				"endpoint": r.RequestURI,
				"request-id": requestID,
				"header":     r.Header,
				"payload":    string(body),
			}).Infoln("incoming request")

		r.Body = ioutil.NopCloser(bytes.NewReader(body))

		next.ServeHTTP(w, r)
	})
}
