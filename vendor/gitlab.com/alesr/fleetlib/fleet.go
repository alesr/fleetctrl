package fleetlib

import (
	"errors"
	"time"
)

// Enumerate user roles.
const (
	UserRoleAdmin  = "admin"
	UserRoleHunter = "hunter"
	UserRoleRider  = "rider"
)

const (
	// Enumerate possible fleet vehicle states.
	stateReady       = "ready"
	stateBatteryLow  = "battery_low"
	stateBounty      = "bounty"
	stateRiding      = "riding"
	stateCollected   = "collected"
	stateDropped     = "dropped"
	stateServiceMode = "service_mode"
	stateTerminated  = "terminated"
	stateUnknown     = "unknown"

	// Enumerate battery levels.

	batteryMaxCap   = 100
	batteryLowLvl   = 20
	batteryRunUsage = 10

	// Enumerate fleet vehicle categories.

	categoryScooter = "scooter"
)

// Enumerate lib errors.
var (
	ErrUnauthorizedUserRole                = errors.New("unauthorized user role")
	ErrUnsupportedUserRole                 = errors.New("unsupported user role")
	ErrUnsupportedVehicleState             = errors.New("unsupported vehicle state")
	ErrUnprocessableVehicleStateTransition = errors.New("unprocessable vehicle state transition")
	ErrVehicleBatteryLevelTooLow           = errors.New("vehicle battery level is too low")
)

var (
	// Enumerate user roles and the fleet vehicle states that can be modified by them.
	userRoleAllowedStates = map[string][]string{
		UserRoleRider: []string{
			stateReady,
			stateRiding,
		},
		UserRoleHunter: []string{
			stateReady,
			stateRiding,
			stateBounty,
			stateCollected,
			stateDropped,
		},
		UserRoleAdmin: []string{
			stateReady,
			stateBatteryLow,
			stateBounty,
			stateRiding,
			stateCollected,
			stateDropped,
			stateServiceMode,
			stateTerminated,
			stateUnknown,
		},
	}

	// Map the available fleet vehicle states with
	// their respective operationability statuses.
	operationalState = map[string]bool{
		stateReady:       true,
		stateBatteryLow:  true,
		stateBounty:      true,
		stateRiding:      true,
		stateCollected:   true,
		stateDropped:     true,
		stateServiceMode: false,
		stateTerminated:  false,
		stateUnknown:     false,
	}

	// Map the a fleet vehicle state to the its transition state.
	// Used for regular user roles (riders & hunters).
	// Admins are free to bypass transition rules and
	// schedulers follow to specify rules.
	stateTransition = map[string]string{
		stateReady:     stateRiding,
		stateRiding:    stateReady,
		stateBounty:    stateCollected,
		stateCollected: stateDropped,
		stateDropped:   stateReady,
	}
)

//go:generate moq -out vehicle_mock.go . Vehicle

// Vehicle defines the interface for a fleet vehicle.
type Vehicle interface {
	BatteryLevel() int
	Category() string
	LastModified() time.Time
	Operational() bool
	SerialNumber() string
	SetAutoBounty()
	SetAutoUnknown()
	SetRandState()
	SetState(userRole, newState string) error
	State() string
}

// UserRoleAllowedVehicleStates returns a list of states that could be displayed to a given user role.
func UserRoleAllowedVehicleStates(userRole string) ([]string, error) {
	states, found := userRoleAllowedStates[userRole]
	if !found {
		return nil, ErrUnsupportedUserRole
	}
	return states, nil
}
