# fleetlib

Fleetlib package implements basic actions around a Fleet vehicle such as setting the vehicle's state following internal rules and getting vehicle information.

## Available commands

```
------------------------------------------------------------------------
FLEETLIB
------------------------------------------------------------------------
build                          compile packages and dependencies
dep                            install mock generator and linter
generate                       generate interface mocks.
lint                           run linter
test                           test packages
tidy                           add missing and remove unused modules
vendor                         make vendored copy of dependencies
```
