package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/require"
)

var baseURL url.URL
var basicAuthUser string
var basicAuthPass string
var client *http.Client

func init() {
	bURL, err := url.Parse(lookupEnvar("FLEETCTRL_BASE_URL"))
	if err != nil {
		log.Fatalf("could not load fleetctrl api base url: %s", err)
	}

	baseURL = *bURL
	basicAuthUser = lookupEnvar("FLEETCTRL_BASIC_AUTH_USER")
	basicAuthPass = lookupEnvar("FLEETCTRL_BASIC_AUTH_PASS")

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client = &http.Client{Transport: tr}
}

func lookupEnvar(envar string) string {
	val, found := os.LookupEnv(envar)
	if !found {
		panic(fmt.Sprintf("could not lookup log level environment variable: '%s'", envar))
	}
	return val
}

func TestBasicAuthFailure(t *testing.T) {
	u := baseURL
	u.Path = path.Join(u.Path, "/vehicles")

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	require.NoError(t, err)

	q := req.URL.Query()
	q.Add("role", "admin")
	req.URL.RawQuery = q.Encode()

	req.SetBasicAuth("invalid_user", "invalid_pass")

	resp, err := client.Do(req)
	require.NoError(t, err)
	defer func() { require.NoError(t, resp.Body.Close()) }()

	require.Equal(t, http.StatusUnauthorized, resp.StatusCode)
}
