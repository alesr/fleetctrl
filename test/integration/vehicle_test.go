package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type listResponse struct {
	Items []itemResponse `json:"vehicles,omitempty"`
}

type itemResponse struct {
	ID           string `json:"id"`
	SerialNumber string `json:"serial_number"`
	Category     string `json:"category"`
	BatteryLevel string `json:"battery_level"`
	State        string `json:"state"`
	Operational  bool   `json:"operational"`
	LastModified string `json:"last_modified"`
}

type setStateRequest struct {
	ID    string `json:"id"`
	State string `json:"state"`
}

type restError struct {
	Code    int    `json:"code"`
	Status  string `json:"status"`
	Message string `json:"message,omitempty"`
}

func TestNewVehicle(t *testing.T) {
	u := baseURL
	u.Path = path.Join(u.Path, "vehicle", "new")

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	require.NoError(t, err)

	q := req.URL.Query()
	q.Add("role", "admin")
	req.URL.RawQuery = q.Encode()

	req.SetBasicAuth(basicAuthUser, basicAuthPass)

	resp, err := client.Do(req)
	require.NoError(t, err)
	defer func() { require.NoError(t, resp.Body.Close()) }()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	var itemResp itemResponse
	dec := json.NewDecoder(resp.Body)
	require.NoError(t, dec.Decode(&itemResp))

	newVehicleID := itemResp.ID

	vehicles := getAllVehiclesHelper(t)

	var found bool
	for _, v := range vehicles.Items {
		if v.ID == newVehicleID {
			found = true
			break
		}
	}

	assert.True(t, found)
}

func TestNewVehicleUnauthorizedUserRole(t *testing.T) {
	u := baseURL
	u.Path = path.Join(u.Path, "vehicle", "new")

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	require.NoError(t, err)

	q := req.URL.Query()
	q.Add("role", "hunter")
	req.URL.RawQuery = q.Encode()

	req.SetBasicAuth(basicAuthUser, basicAuthPass)

	resp, err := client.Do(req)
	require.NoError(t, err)
	defer func() { require.NoError(t, resp.Body.Close()) }()

	require.Equal(t, http.StatusForbidden, resp.StatusCode)

	var restErr restError
	dec := json.NewDecoder(resp.Body)
	require.NoError(t, dec.Decode(&restErr))

	assert.Equal(t, "unauthorized user role", restErr.Message)
}

func TestGetVehicle(t *testing.T) {
	vehicle := createVehicleHelper(t)

	u := baseURL
	u.Path = path.Join(u.Path, "vehicle", vehicle.ID)

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	require.NoError(t, err)

	q := req.URL.Query()
	q.Add("role", "admin")
	req.URL.RawQuery = q.Encode()

	req.SetBasicAuth(basicAuthUser, basicAuthPass)

	resp, err := client.Do(req)
	require.NoError(t, err)
	defer func() { require.NoError(t, resp.Body.Close()) }()

	assert.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestListVehicles(t *testing.T) {
	u := baseURL
	u.Path = path.Join(u.Path, "/vehicles")

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	require.NoError(t, err)

	q := req.URL.Query()
	q.Add("role", "admin")
	req.URL.RawQuery = q.Encode()

	req.SetBasicAuth(basicAuthUser, basicAuthPass)

	resp, err := client.Do(req)
	require.NoError(t, err)
	defer func() { require.NoError(t, resp.Body.Close()) }()

	assert.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestSetVehicleState(t *testing.T) {
	newState := "bounty"

	vehicle := createVehicleHelper(t) // "service_mode" default

	u := baseURL
	u.Path = path.Join(u.Path, "vehicle", "state")

	setStateReq := setStateRequest{
		ID:    vehicle.ID,
		State: newState,
	}

	body, err := json.Marshal(setStateReq)
	require.NoError(t, err)

	req, err := http.NewRequest(http.MethodPatch, u.String(), bytes.NewReader(body))
	require.NoError(t, err)

	q := req.URL.Query()
	q.Add("role", "admin")
	req.URL.RawQuery = q.Encode()

	req.SetBasicAuth(basicAuthUser, basicAuthPass)

	resp, err := client.Do(req)
	require.NoError(t, err)
	defer func() { require.NoError(t, resp.Body.Close()) }()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	vehicleWithNewState := getVehicleHelper(t, vehicle.ID)
	assert.Equal(t, newState, vehicleWithNewState.State)
}
