module gitlab.com/alesr/fleetctrl/test/integration

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/magiconair/properties v1.8.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
)
