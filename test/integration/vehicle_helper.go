package main

import (
	"encoding/json"
	"net/http"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func createVehicleHelper(t *testing.T) itemResponse {
	u := baseURL
	u.Path = path.Join(u.Path, "vehicle", "new")

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	require.NoError(t, err)

	q := req.URL.Query()
	q.Add("role", "admin")
	req.URL.RawQuery = q.Encode()

	req.SetBasicAuth(basicAuthUser, basicAuthPass)

	resp, err := client.Do(req)
	require.NoError(t, err)
	defer func() { require.NoError(t, resp.Body.Close()) }()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	var itemResp itemResponse
	dec := json.NewDecoder(resp.Body)
	require.NoError(t, dec.Decode(&itemResp))

	return itemResp
}

func getAllVehiclesHelper(t *testing.T) listResponse {
	u := baseURL
	u.Path = path.Join(u.Path, "/vehicles")

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	require.NoError(t, err)

	q := req.URL.Query()
	q.Add("role", "admin")
	req.URL.RawQuery = q.Encode()

	req.SetBasicAuth(basicAuthUser, basicAuthPass)

	resp, err := client.Do(req)
	require.NoError(t, err)
	defer func() { require.NoError(t, resp.Body.Close()) }()

	assert.Equal(t, http.StatusOK, resp.StatusCode)

	var listResp listResponse
	dec := json.NewDecoder(resp.Body)
	require.NoError(t, dec.Decode(&listResp))

	return listResp
}

func getVehicleHelper(t *testing.T, id string) itemResponse {
	u := baseURL
	u.Path = path.Join(u.Path, "vehicle", id)

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	require.NoError(t, err)

	q := req.URL.Query()
	q.Add("role", "admin")
	req.URL.RawQuery = q.Encode()

	req.SetBasicAuth(basicAuthUser, basicAuthPass)

	resp, err := client.Do(req)
	require.NoError(t, err)
	defer func() { require.NoError(t, resp.Body.Close()) }()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	var itemResp itemResponse
	dec := json.NewDecoder(resp.Body)
	require.NoError(t, dec.Decode(&itemResp))

	return itemResp
}
